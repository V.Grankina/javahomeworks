import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String[][] gameField = {
                {"-", "-", "-", "-", "-"},
                {"-", "-", "-", "-", "-"},
                {"-", "-", "-", "-", "-"},
                {"-", "-", "-", "-", "-"},
                {"-", "-", "-", "-", "-"},
        };
        System.out.println("All set. Get ready to rumble!");
        printGameField(gameField);
        int[][] target = getRandomLine();
        int userTargetX, userTargetY;
        int strikeCounter = 0;
        do {
            userTargetX = getUserInt("X");
            userTargetY = getUserInt("Y");
            if(gameField[userTargetY][userTargetX].equals("X") || gameField[userTargetY][userTargetX].equals("*")){
                System.out.println("You've already shoot this cell!");
            }
            if (checkUsersStrike(userTargetX, userTargetY, target)) {
                strikeCounter++;
                gameField[userTargetY][userTargetX] = "X";
                System.out.println("You got it!");
            } else {
                gameField[userTargetY][userTargetX] = "*";
                System.out.println("Try again!");
            }
            printGameField(gameField);
        } while (strikeCounter != 3);
        System.out.println("You win!");
    }

    public static int[][] getRandomLine() {
        Random random = new Random();
        int axis = random.nextInt(2);
        int targetLine = random.nextInt(5);
        int targetStroke = random.nextInt(5);
        if (axis == 0) {
            if (targetStroke < 3) {
                return new int[][]{
                        {targetLine},
                        {targetStroke, targetStroke + 1, targetStroke + 2}
                };
            } else {
                return new int[][]{
                        {targetLine},
                        {targetStroke - 2, targetStroke - 1, targetStroke}
                };
            }
        }
        if (axis == 1) {
            if (targetLine < 3) {
                return new int[][]{
                        {targetLine, targetLine + 1, targetLine + 2},
                        {targetStroke}
                };
            } else {
                return new int[][]{
                        {targetLine - 2, targetLine - 1, targetLine},
                        {targetStroke}
                };
            }
        }
        return new int[0][0];
    }

    public static int getUserInt(String axis) {
        Scanner in = new Scanner(System.in);
        String userIntString;
        int userInt = 0;
        System.out.printf("Enter target on %s-axis between 1 and 5\n", axis);
        do {
            userIntString = in.nextLine();
            if (!userIntString.matches("^\\d$")) {
                System.out.println("You must enter a number between 1 and 5");
                continue;
            }
            userInt = Integer.parseInt(userIntString);
            if (userInt <= 0 || userInt > 5) {
                System.out.println("You must enter a number between 1 and 5");
            }
        } while (!userIntString.matches("^\\d$") || userInt <= 0 || userInt > 5);
        return userInt - 1;
    }

    public static void printGameField(String[][] gameField) {
        System.out.println("0 | 1 | 2 | 3 | 4 | 5 |");
        for (int i = 0; i < gameField.length; i++) {
            System.out.printf("%d |", i + 1);
            for (String value : gameField[i]) {
                System.out.printf(" %s |", value);
            }
            System.out.println();
        }
    }

    public static boolean checkUsersStrike(int userTargetX, int userTargetY, int[][] target) {
        boolean xAxis = false;
        boolean yAxis = false;
        for (int lineY : target[0]) {
            if (lineY == userTargetY) {
                yAxis = true;
                break;
            }
        }
        for (int lineX : target[1]) {
            if (lineX == userTargetX) {
                xAxis = true;
                break;
            }
        }
        return yAxis && xAxis;
    }
}