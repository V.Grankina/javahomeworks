package structure;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class Logger {
    private File loggerFile = new File("application.log");

    public  void info(String message){
        if (!loggerFile.exists()) {
            System.out.println("file is not found");
            System.exit(0);
        }
        FileWriter fileWriter = null;
        Calendar calendar = new GregorianCalendar();
        try {
            fileWriter = new FileWriter(loggerFile, true);
            fileWriter.write(String.valueOf(calendar.getTime()) + " [INFO] " + message + "\n");
            fileWriter.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    public void error(String message){
        if (!loggerFile.exists()) {
            System.out.println("file is not found");
            System.exit(0);
        }
        FileWriter fileWriter = null;
        Calendar calendar = new GregorianCalendar();
        try {
            fileWriter = new FileWriter(loggerFile, true);
            fileWriter.write(String.valueOf(calendar.getTime()) + " [ERROR] " + message + "\n");
            fileWriter.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}
