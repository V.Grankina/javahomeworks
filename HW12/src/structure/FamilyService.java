package structure;

import exceptions.ToMuchKidsException;
import happyFamily.Family;
import happyFamily.humans.Human;
import happyFamily.humans.HumanCreator;
import happyFamily.humans.Man;
import happyFamily.humans.Women;
import happyFamily.pets.Pet;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

public class FamilyService {
    private FamilyDao familyDao;

    public FamilyService(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public void displayAllFamilies() {
        familyDao.getAllFamilies().forEach(family -> family.prettyFormat());
    }

    public List<Family> getFamiliesBiggerThan(int familyMembers) {
        List<Family> families = familyDao.getAllFamilies();
        return families.stream().filter(family -> family.countFamily() > familyMembers).toList();
    }

    public List<Family> getFamiliesLessThan(int familyMembers) {
        List<Family> families = familyDao.getAllFamilies();
        return families.stream().filter(family -> family.countFamily() < familyMembers).toList();

    }

    public int countFamiliesWithMemberNumber(int familyMembers) {
        List<Family> families = familyDao.getAllFamilies();
        return (int) families.stream().filter(family -> family.countFamily() == familyMembers).count();
    }

    public void createNewFamily(Women Parent1, Man Parent2) {
        Family family = new Family(Parent1, Parent2);
        familyDao.saveFamily(family);
    }

    public boolean deleteFamilyByIndex(int index) {
        return familyDao.deleteFamily(index);
    }

    public Family bornChild(Family family) throws ToMuchKidsException {
        if (familyDao.checkIfContainsFamily(family)) {
                if (family.getChildren().size() == 10) {
                    throw new ToMuchKidsException("В семье не может быть больше 10 детей!");
                }
                HumanCreator.bornChild(family);
        }
        return family;
    }

    public void adoptChild  (Family family, Human kid) throws ToMuchKidsException {
        if (familyDao.checkIfContainsFamily(family)) {
            if (family.getChildren().size() == 10) {
                throw new ToMuchKidsException("В семье не может быть больше 10 детей!");
            }
            family.addChild(kid);
        }
    }

    public void deleteAllChildrenOlderThen(int age) {
        List<Family> families = familyDao.getAllFamilies();
        families.forEach(family -> family.deleteAllChildrenOlderThen(age));
    }

    public int count() {
        return familyDao.getAllFamilies().size();
    }

    public Family getFamilyById(int id) {
        return familyDao.getFamilyByIndex(id);
    }

    public Set<Pet> getPets(int index) {
        Family family = familyDao.getFamilyByIndex(index);
        return family.getPets();
    }

    public void addPet(int index, Pet pet) {
        Family family = familyDao.getFamilyByIndex(index);
        family.addPet(pet);
    }

    public void updateAllFamilies(List<Family> families) {
        familyDao.updateAllFamilies(families);
    }

    public void loadData(List<Family> families) {
        familyDao.loadData(families);
    }
}
