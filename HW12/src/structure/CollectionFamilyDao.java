package structure;

import happyFamily.Family;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;

    public class CollectionFamilyDao implements FamilyDao{

    private List<Family> FamilyList;
    private Logger logger = new Logger();

    public CollectionFamilyDao(List<Family> familyList) {
        FamilyList = familyList;
    }


    @Override
    public List<Family> getAllFamilies() {
        logger.info("получение списка семей");
        return FamilyList;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        logger.info("получение семьи по индексу");
        return FamilyList.get(index);
    }

    @Override
    public boolean deleteFamily(int index) {
        logger.info("удаление семьи");
        if (index < 0 || index >= FamilyList.size()){
            return false;
        }
        FamilyList.remove(index);
        return true;
    }

    @Override
    public boolean deleteFamily(Family family) {
        logger.info("удаление семьи");
        return FamilyList.remove(family);
    }

    @Override
    public void saveFamily(Family family) {
        logger.info("добавление новой семьи");
        FamilyList.add(family);
    }


    @Override
    public boolean checkIfContainsFamily(Family family) {
        logger.info("проверка на наличие семьи");
        return FamilyList.contains(family);
    }

    @Override
    public void updateAllFamilies(List<Family> families) {
        logger.info("обновление списка семей");
        this.FamilyList = families;
    }

        @Override
        public void loadData(List<Family> families) {
            try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("families"))) {
                objectOutputStream.writeObject(families);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
