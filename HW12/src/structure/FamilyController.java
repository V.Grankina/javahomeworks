package structure;

import happyFamily.Family;
import happyFamily.humans.Human;
import happyFamily.humans.Man;
import happyFamily.humans.Women;
import happyFamily.pets.Pet;

import java.io.*;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class FamilyController {
    private FamilyService familyService;

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;
    }

    public void start() {
        while (true) {
            System.out.println("Выберите номер команды:");
            System.out.println("1. Заполнить тестовыми данными");
            System.out.println("2. Отобразить весь список семей");
            System.out.println("3. Отобразить список семей, где количество людей больше заданного");
            System.out.println("4. Отобразить список семей, где количество людей меньше заданного");
            System.out.println("5. Подсчитать количество семей, где количество членов равно");
            System.out.println("6. Создать новую семью");
            System.out.println("7. Удалить семью по индексу семьи в общем списке");
            System.out.println("8. Редактировать семью по индексу семьи в общем списке");
            System.out.println("9. Удалить всех детей старше возраста");
            System.out.println("10. Выход");
            System.out.println("Ваш выбор:");
            int menuItem = this.getUserInt(10);
            switch (menuItem) {
                case 1:
                    System.out.println("Заполняю базу данных тестовыми данными из файла");
                    List <Family> families;
                    try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("families"))) {
                        families = (List<Family>) objectInputStream.readObject();
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    } catch (ClassNotFoundException e) {
                        throw new RuntimeException(e);
                    }
                    familyService.updateAllFamilies(families);
                    System.out.println("База данных обновлена!");
                    break;
                case 2:
                    System.out.println("Вот список семей:");
                    this.displayAllFamilies();
                    break;
                case 3:
                    System.out.println("Введите минимально допусимое значение:");
                    int number = this.getUserInt(25);
                    List<Family> familiesBiggerThan = this.getFamiliesBiggerThan(number);
                    if (familiesBiggerThan.size() == 0) {
                        System.out.println("Ничего не найдено");
                    }
                    familiesBiggerThan.forEach(family -> family.prettyFormat());
                    break;
                case 4:
                    System.out.println("Введите максимально допусимое значение:");
                    int number1 = this.getUserInt(25);
                    List<Family> familiesLessThan = this.getFamiliesLessThan(number1);
                    if (familiesLessThan.size() == 0) {
                        System.out.println("Ничего не найдено");
                    }
                    familiesLessThan.forEach(family -> family.prettyFormat());
                    break;
                case 5:
                    System.out.println("Введите значение:");
                    int number2 = this.getUserInt(25);
                    int familiesWithMemberNumber = this.countFamiliesWithMemberNumber(number2);
                    if (familiesWithMemberNumber == 0) {
                        System.out.println("Ничего не найдено");
                    }
                    System.out.println("Есть " + familiesWithMemberNumber + " семей с " + number2 + " членами");
                    break;
                case 6:
                    System.out.println("Введите имя матери");
                    String mothersName = this.getUserString("имя матери");
                    System.out.println("Введите фамилию матери");
                    String mothersSurname = this.getUserString("фамилию матери");
                    System.out.println("Введите год рождения матери");
                    int motherBirthYear = this.getUserInt(2005);
                    System.out.println("Введите месяц рождения матери");
                    int motherBirthMonth = this.getUserInt(12);
                    System.out.println("Введите день рождения матери");
                    int motherBirthDay = this.getUserInt(31);
                    System.out.println("Введите iq матери");
                    int motherIq = this.getUserInt(100);
                    String motherBirthDate = String.format("%s/%s/%d",
                            motherBirthDay < 10 ? "0" + motherBirthDay : String.valueOf(motherBirthDay),
                            motherBirthMonth < 10 ? "0" + motherBirthMonth : String.valueOf(motherBirthMonth),
                            motherBirthYear);
                    Women mother = new Women(mothersName, mothersSurname, motherBirthDate, motherIq);
                    System.out.println("Введите имя отца");
                    String fatherName = this.getUserString("имя отца");
                    System.out.println("Введите фамилию отца");
                    String fatherSurname = this.getUserString("фамилию отца");
                    System.out.println("Введите год рождения отца");
                    int fatherBirthYear = this.getUserInt(2005);
                    System.out.println("Введите месяц рождения отца");
                    int fatherBirthMonth = this.getUserInt(12);
                    System.out.println("Введите день рождения отца");
                    int fatherBirthDay = this.getUserInt(31);
                    System.out.println("Введите iq отца");
                    int fatherIq = this.getUserInt(100);
                    String fatherBirthDate = String.format("%s/%s/%d",
                            fatherBirthDay < 10 ? "0" + fatherBirthDay : String.valueOf(fatherBirthDay),
                            fatherBirthMonth < 10 ? "0" + fatherBirthMonth : String.valueOf(fatherBirthMonth),
                            fatherBirthYear);
                    Man father = new Man(fatherName, fatherSurname, fatherBirthDate, fatherIq);
                    this.createNewFamily(mother, father);
                    List <Family> families1 = familyService.getAllFamilies();
                    familyService.loadData(familyService.getAllFamilies());
                    break;
                case 7:
                    System.out.println("Введите индекс семьи, которую необходимо удалить:");
                    int indexOfFamilyToDelete = this.getUserInt(this.familyService.getAllFamilies().size() - 1);
                    this.deleteFamilyByIndex(indexOfFamilyToDelete);
                    familyService.loadData(familyService.getAllFamilies());
                    break;
                case 8:
                    System.out.println("Выберите вариант редактирования:");
                    System.out.println("1. Родить ребенка");
                    System.out.println("2. Усыновить ребенка");
                    System.out.println("3. Вернуться в главное меню");
                    int userChoiceForCase8 = this.getUserInt(3);
                    switch (userChoiceForCase8) {
                        case 1:
                            System.out.println("Введите индекс семьи, в которой родится ребенок:");
                            int indexOfFamilyToBurnChild = this.getUserInt(this.familyService.getAllFamilies().size());
                            Family familyToBurnChild = this.getFamilyById(indexOfFamilyToBurnChild - 1);
                            try{
                                this.bornChild(familyToBurnChild);
                                System.out.println("В семье номер " + indexOfFamilyToBurnChild + " родился ребенок!");
                            } catch (Exception exception){
                                System.out.println(exception);
                            }

                            break;
                        case 2:
                            System.out.println("Введите индекс семьи, которая усыновит ребенка:");
                            int indexOfFamilyToAdoptChild = this.getUserInt(this.familyService.getAllFamilies().size() - 1);
                            Family familyToAdoptChild = this.getFamilyById(indexOfFamilyToAdoptChild);
                            System.out.println("Выберите пол ребенка (1 - девочка, 2 - мальчик");
                            int kidGender = this.getUserInt(2);
                            System.out.println("Введите имя ребенка");
                            String kidName = this.getUserString("имя ребенка");
                            System.out.println("Введите фамилию ребенка");
                            String kidSurname = this.getUserString("фамилию ребенка");
                            System.out.println("Введите год рождения ребенка");
                            int kidBirthYear = this.getUserInt(2023);
                            System.out.println("Введите месяц рождения ребенка");
                            int kidBirthMonth = this.getUserInt(12);
                            System.out.println("Введите день рождения ребенка");
                            int kidBirthDay = this.getUserInt(31);
                            System.out.println("Введите iq ребенка");
                            int kidIq = this.getUserInt(100);
                            String kidBirthDate = String.format("%s/%s/%d",
                                    kidBirthDay < 10 ? "0" + kidBirthDay : String.valueOf(kidBirthDay),
                                    kidBirthMonth < 10 ? "0" + kidBirthMonth : String.valueOf(kidBirthMonth),
                                    kidBirthYear);
                            switch (kidGender) {
                                case 1:
                                    Women girl = new Women(kidName, kidSurname, kidBirthDate, kidIq);
                                    try {
                                        this.adoptChild(familyToAdoptChild, girl);
                                        System.out.println("В семье номер " + indexOfFamilyToAdoptChild + " появился ребенок!");
                                    } catch (Exception exception){
                                        System.out.println(exception);
                                    }
                                    break;
                                case 2:
                                    Man boy = new Man(kidName, kidSurname, kidBirthDate, kidIq);
                                    try {
                                        this.adoptChild(familyToAdoptChild, boy);
                                        System.out.println("В семье номер " + indexOfFamilyToAdoptChild + " появился ребенок!");
                                    } catch (Exception exception){
                                        System.out.println(exception);
                                    }
                                    break;
                            }
                            break;
                        case 3:
                            break;
                        default:
                            System.out.println("Введите число, соответсвующее пункту меню!");
                            break;
                    }
                    familyService.loadData(familyService.getAllFamilies());
                    break;
                case 9:
                    System.out.println("Введите возраст, старше которого дети будут удалены");
                    int maxAge = this.getUserInt(100);
                    this.deleteAllChildrenOlderThen(maxAge);
                    System.out.println("Все дети старше " + maxAge + " лет удалены");
                    familyService.updateAllFamilies(familyService.getAllFamilies());
                    break;
                case 10:
                    System.exit(0);
                default:
                    System.out.println("Введите число, соответсвующее пункту меню!");
                    break;
            }
        }
    }

    int getUserInt(int max) {
        Scanner in = new Scanner(System.in);
        String userIntString;
        int userInt = 0;
        do {
            userIntString = in.nextLine();
            if (!userIntString.matches("^[1-9]+[0-9]*$")) {
                System.out.println("Введите целое положительное число");
                continue;
            }
            userInt = Integer.parseInt(userIntString);
            if (userInt <= 0 || userInt > max) {
                System.out.println("Введите целое положительное число не больше " + max);
            }
        } while (!userIntString.matches("^[1-9]+[0-9]*$") || userInt <= 0 || userInt > max);
        return userInt;
    }

    String getUserString(String returnedValue) {
        Scanner in = new Scanner(System.in);
        String userString;
        String ruNameRegex = "[a-zA-Z]+";
        do {
            userString = in.nextLine();
            String userStringTrimmed = userString.trim();
            userString = userStringTrimmed.substring(0, 1).toUpperCase() + userStringTrimmed.substring(1);
            if (!userString.matches(ruNameRegex)) {
                System.out.println("Введите " + returnedValue);
                continue;
            }
        } while (!userString.matches(ruNameRegex));
        return userString;
    }

    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    public void displayAllFamilies() {
        familyService.displayAllFamilies();
    }

    public List<Family> getFamiliesBiggerThan(int familyMembers) {
        return familyService.getFamiliesBiggerThan(familyMembers);
    }

    public List<Family> getFamiliesLessThan(int familyMembers) {
        return familyService.getFamiliesLessThan(familyMembers);
    }

    public int countFamiliesWithMemberNumber(int familyMembers) {
        return familyService.countFamiliesWithMemberNumber(familyMembers);
    }

    public void createNewFamily(Women Parent1, Man Parent2) {
        familyService.createNewFamily(Parent1, Parent2);
    }

    public boolean deleteFamilyByIndex(int index) {
        return familyService.deleteFamilyByIndex(index);
    }

    public Family bornChild(Family family) throws Exception {
        return familyService.bornChild(family);
    }

    public void adoptChild(Family family, Human kid) throws Exception {
        familyService.adoptChild(family, kid);
    }

    public void deleteAllChildrenOlderThen(int age) {
        familyService.deleteAllChildrenOlderThen(age);
    }

    public int count() {
        return familyService.count();
    }

    public Family getFamilyById(int id) {
        return familyService.getFamilyById(id);
    }

    public Set<Pet> getPets(int index) {
        return familyService.getPets(index);
    }

    public void addPet(int index, Pet pet) {
        familyService.addPet(index, pet);
    }
}