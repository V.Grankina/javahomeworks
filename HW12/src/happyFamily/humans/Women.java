package happyFamily.humans;

import happyFamily.DayOfWeek;

import java.util.Map;

final public class Women extends Human {
    public Women() {
    }

    public Women(String name, String surname, String birthDate) {
        super(name, surname, birthDate);
    }

    public Women(String name, String surname, String birthDate, Human mother, Human father) {
        super(name, surname, birthDate, mother, father);
    }

    public Women(String name, String surname, long birthDate, Human mother, Human father) {
        super(name, surname, birthDate, mother, father);
    }

    public Women(String name, String surname, String birthdate, int iq) {
        super(name, surname, birthdate, iq);
    }

    public Women(String name, String surname, String birthdate, int iq, Human mother, Human father, Map<DayOfWeek, String> schedule) {
        super(name, surname, birthdate, iq, mother, father, schedule);
    }

    public void cleanTheHouse (){
        System.out.println("Люблю когда чисто");
    }
}
