package tests;
import happyFamily.humans.Human;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class HumanTests {
    private Human module;

    @BeforeEach
    public void setUp() {
        module = new Human();
    }


    @Test
    public void testEquals(){
        Human human1 = new Human("Test", "Equals", "01/01/2022");
        Human human2 = new Human("Test", "Equals", "01/01/2022");
        Assertions.assertEquals(human1, human2);
        int human1HashCode = human1.hashCode();
        int human2HashCode = human2.hashCode();
        assertEquals(human1HashCode, human2HashCode);
        assertEquals(human1HashCode, human1.hashCode());
    }

}
