package exceptions;

public class ToMuchKidsException extends Exception{

    public ToMuchKidsException() {
    }

    public ToMuchKidsException(String message) {
        super(message);
    }
}
