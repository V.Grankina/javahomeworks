package happyFamily;

import java.util.Arrays;
import java.util.Random;

public class Family {
    private final Human mother;
    private final Human father;
    private Human[] children;
    private Pet pet;

    public Human getMother() {
        return mother;
    }

    public Human getFather() {
        return father;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        this.mother.setFamily(this);
        this.father.setFamily(this);
    }

    @Override
    public String toString() {
        return String.format("Father: %sMother: %sChildren: %s\nPet: %s",
                this.father.toString(),
                this.mother.toString(),
                this.children == null ? "null" : Arrays.deepToString(children),
                this.getPet() == null ? "null" : this.getPet().toString());
    }

    public void greetPet() {
        System.out.printf("Привет, %s\n", this.getPet().getNickname());
    }

    public void describePet() {
        int trickLevel = this.getPet().getTrickLevel();
        String trickLevelString = trickLevel > 50 ? "очень хитрый" : "почти не хитрый";
        System.out.printf("У нас есть %s, ему %d лет, он %s\n", this.getPet().getSpecies(), this.getPet().getAge(), trickLevelString);
    }

    public void addChild(Human child) {
        Human[] newChildren;
        if (this.getChildren() == null) {
            newChildren = new Human[]{child};
        } else {
            newChildren = Arrays.copyOf(this.getChildren(), this.children.length + 1);
            newChildren[newChildren.length - 1] = child;
        }
        child.setFamily(this);
        child.setFather(this.father);
        child.setMother(this.mother);
        this.setChildren(newChildren);
    }

    public boolean deleteChild(Human child) {
        if (this.children == null) {
            return false;
        }
        Human[] previousChildren = this.getChildren().clone();
        Human[] newChildren;
        for (Human kid : previousChildren) {
            if (kid.equals(child)) {
                newChildren = new Human[this.children.length - 1];
                int j = 0;
                for (Human previousChild : previousChildren) {
                    if (previousChild.equals(child)) {
                        continue;
                    }
                    newChildren[j] = previousChild;
                    j++;
                }
                this.setChildren(newChildren);
                return true;
            }
        }
        return false;
    }

    public boolean feedPet(boolean timeToFeed) {
        if (!timeToFeed) {
            System.out.printf("Думаю, %s не голоден.\n", this.pet.getNickname());
            return false;
        }
        Random random = new Random();
        int randomInt = random.nextInt(100);
        if (this.pet.getTrickLevel() < randomInt) {
            System.out.printf("Думаю, %s не голоден.\n", this.pet.getNickname());
            return false;
        } else {
            System.out.printf("Хм... покормлю ка я %s\n", this.pet.getNickname());
            return true;
        }
    }

    public int countFamily(){
       return 2 + (this.children == null ? 0 : this.children.length);
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println(this.toString());
        super.finalize();
    }

}
