import happyFamily.*;

public class Main {
    public static void main(String[] args) {
        String[][] schedule = {
                {DayOfWeek.SUNDAY.name(), "find a new hobby"},
                {DayOfWeek.MONDAY.name(), "leave that hobby couse you need to earn money"},
                {DayOfWeek.THURSDAY.name(), "sleep"},
                {DayOfWeek.WEDNESDAY.name(), "earn more money"},
                {DayOfWeek.THURSDAY.name(), "find a friend"},
                {DayOfWeek.FRIDAY.name(), "go to the bar"},
                {DayOfWeek.SATURDAY.name(), "relax"}
        };
        Pet pet = new Pet(Species.PARROT, "Fly");
        System.out.println(pet);

        for (int i = 0; i < 1000000; i++) {
            Human human = new Human("Try", "Finalize", i);
        }


    }
}