package tests;

import happyFamily.Family;
import happyFamily.Human;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class FamilyTests {
    private Family module;
    private Human mother = new Human("Mother", "Mother", 1990);
    private Human father = new Human("Father", "Father", 1980);
    private Human kid1 = new Human("kid1", "kid1", 2000);
    private Human kid2 = new Human("kid2", "kid2", 2002);
    private Human kid3 = new Human("kid3", "kid3", 2005);

    private Human [] children = {kid1, kid2, kid3};

    @BeforeEach
    public void setUp(){
        module = new Family(mother, father);
    }

    @Test
    public void testDeleteChild(){
        module.setChildren(this.children);
        module.deleteChild(kid1);
        int actual = module.getChildren().length;
        int expected = 2;
        Assertions.assertEquals(actual, expected);
    }

    @Test
    public void testDontDeleteChild(){
        module.setChildren(this.children);
        module.deleteChild(new Human());
        int actual = module.getChildren().length;
        int expected = 3;
        Assertions.assertEquals(actual, expected);
    }

    @Test
    public void testAddChild(){
        module.setChildren(this.children);
        module.addChild(new Human());
        int actual = module.getChildren().length;
        int expected = 4;
        Assertions.assertEquals(actual, expected);
    }
    @Test
    public void testCountFamily(){
        module.setChildren(this.children);
        int actual = module.countFamily();;
        int expected = 5;
        Assertions.assertEquals(actual, expected);
    }
    @Test
    public void testToString(){
        String actual = module.toString();
        String expected = "Father: Human {name = 'Father', surname = 'Father', year = 1980, iq = 0, mother = null, father = null}\n" +
                "Mother: Human {name = 'Mother', surname = 'Mother', year = 1990, iq = 0, mother = null, father = null}\n" +
                "Children: null\n" +
                "Pet: null";
        Assertions.assertEquals(actual, expected);
    }
}
