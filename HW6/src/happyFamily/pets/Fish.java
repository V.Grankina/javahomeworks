package happyFamily.pets;

import happyFamily.Species;

public class Fish extends Pet {
    Species species;

    {
        species = Species.FISH;
    }

    public Fish() {
    }

    public Fish(String nickname) {
        super(nickname);
    }

    public Fish(String nickname, byte age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.println("Буль-буль");
    }
}
