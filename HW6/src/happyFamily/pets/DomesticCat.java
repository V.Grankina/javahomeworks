package happyFamily.pets;

import happyFamily.Species;

public class DomesticCat extends Pet implements Fouling{
    Species species;

    {
        species = Species.CAT;
    }

    public DomesticCat() {
    }

    public DomesticCat(String nickname) {
        super(nickname);
    }

    public DomesticCat(String nickname, byte age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void foul() {
        System.out.println("Я кот это не мои проблемы. Кто кота чесал, тому и убираться");
    }

    @Override
    public void respond() {
        System.out.println("Мяу мяу");
    }
}
