package happyFamily.pets;

import happyFamily.Species;

public class Dog extends Pet implements Fouling{
    Species species;

    {
        species = Species.DOG;
    }

    public Dog() {
    }

    public Dog(String nickname) {
        super(nickname);
    }

    public Dog(String nickname, byte age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void foul() {
        System.out.println("Это кот сделал");
    }

    @Override
    public void respond() {
        System.out.println("Гав гав гав");
    }
}
