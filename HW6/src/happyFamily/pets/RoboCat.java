package happyFamily.pets;

import happyFamily.Species;

public class RoboCat extends Pet{
    Species species;

    {
        species = Species.ROBOCAT;
    }

    public RoboCat() {
    }

    public RoboCat(String nickname) {
        super(nickname);
    }

    public RoboCat(String nickname, byte age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.println("0101100101");
    }
}
