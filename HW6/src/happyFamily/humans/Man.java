package happyFamily.humans;

final public class Man extends Human{
    public Man() {
    }

    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Man(String name, String surname, int year, int iq) {
        super(name, surname, year, iq);
    }

    public Man(String name, String surname, int year, Human mother, Human father) {
        super(name, surname, year, mother, father);
    }

    public Man(String name, String surname, int year, int iq, Human mother, Human father, String[][] schedule) {
        super(name, surname, year, iq, mother, father, schedule);
    }
    public void DriveTheCar(){
        System.out.println("Би бип");
    }
}
