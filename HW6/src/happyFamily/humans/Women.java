package happyFamily.humans;

final public class Women extends Human {
    public Women() {
    }

    public Women(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Women(String name, String surname, int year, Human mother, Human father) {
        super(name, surname, year, mother, father);
    }

    public Women(String name, String surname, int year, int iq) {
        super(name, surname, year, iq);
    }

    public Women(String name, String surname, int year, int iq, Human mother, Human father, String[][] schedule) {
        super(name, surname, year, iq, mother, father, schedule);
    }

    public void cleanTheHouse (){
        System.out.println("Люблю когда чисто");
    }
}
