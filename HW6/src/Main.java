import happyFamily.*;
import happyFamily.humans.Human;
import happyFamily.humans.HumanCreator;
import happyFamily.humans.Man;
import happyFamily.humans.Women;
import happyFamily.pets.Fish;
import happyFamily.pets.Pet;

public class Main {
    public static void main(String[] args) {
        String[][] schedule = {
                {DayOfWeek.SUNDAY.name(), "find a new hobby"},
                {DayOfWeek.MONDAY.name(), "leave that hobby couse you need to earn money"},
                {DayOfWeek.THURSDAY.name(), "sleep"},
                {DayOfWeek.WEDNESDAY.name(), "earn more money"},
                {DayOfWeek.THURSDAY.name(), "find a friend"},
                {DayOfWeek.FRIDAY.name(), "go to the bar"},
                {DayOfWeek.SATURDAY.name(), "relax"}
        };

        Pet fish = new Fish();
        fish.respond();

        Women mother = new Women("Mother", "Mother", 1990, 78);
        Man father = new Man("Father", "Father", 1980, 87);
        Family family = new Family<>(mother, father);
        Human kid = HumanCreator.bornChild(family);
        System.out.println(kid);

    }
}