import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String[][] mainHPDates = {
                {"1965", "1980", "1981", "1991", "1996", "1998"},
                {"When is Joan J. K. Rowling's birthday?",
                        "When is Harry Potter's birthday?",
                        "When did James and Lily died?",
                        "When Harry Potter got his first Hogwarts letter?",
                        "When died Sirius Black?",
                        "When was the main Battle for Hogwarts?",
                }
        };
        Random random = new Random();
        int randomInt = random.nextInt(6);
        Scanner in = new Scanner(System.in);
        System.out.println("Let the game begin!");
        System.out.println("Please, enter your name");
        String name = in.nextLine();
        System.out.println(mainHPDates[1][randomInt]);
        int yearInt = 0;
        int answer = 0;
        String year;
        int[] usersAnswersArray = new int[0];
        do {
            year = in.nextLine();
            if (!year.matches("^\\d{4}$")) {
                System.out.println("You mast enter a year!");
                continue;
            }
            yearInt = Integer.parseInt(year);
            answer = Integer.parseInt(mainHPDates[0][randomInt]);
            if (yearInt < answer) {
                System.out.println("It was later");
                usersAnswersArray = increaseArray(usersAnswersArray, yearInt);
            }
            if (yearInt > answer) {
                System.out.println("It was earlier");
                usersAnswersArray = increaseArray(usersAnswersArray, yearInt);
            }
        } while (!year.matches("^\\d{4}$") || yearInt != answer);
        System.out.printf("Congratulations, %s!\n", name);
        if (usersAnswersArray.length > 0){
            sortArray(usersAnswersArray);
            System.out.printf("Your previous answers are: " + Arrays.toString(usersAnswersArray));
        }
    }

    public static int[] increaseArray(int[] prevArray, int newValue) {
        int[] newArray = Arrays.copyOf(prevArray, prevArray.length + 1);
        newArray[newArray.length - 1] = newValue;
        return newArray;
    }
    public static void sortArray(int[] unsortedArray) {
        int n = unsortedArray.length;
        int temp;
        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {
                if (unsortedArray[j - 1] > unsortedArray[j]) {
                    temp = unsortedArray[j - 1];
                    unsortedArray[j - 1] = unsortedArray[j];
                    unsortedArray[j] = temp;
                }
            }
        }
    }
}