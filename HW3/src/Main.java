import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String[][] schedule = new String[7][2];
        schedule[0][0] = "Sunday";
        schedule[0][1] = "do home work";
        schedule[1][0] = "Monday";
        schedule[1][1] = "go to courses; watch a film";
        schedule[2][0] = "Tuesday";
        schedule[2][1] = "do the laundry, clean the bath";
        schedule[3][0] = "Wednesday";
        schedule[3][1] = "watch the Wednesday on Netflix";
        schedule[4][0] = "Thursday";
        schedule[4][1] = "Nothing screws up your Friday like realizing it's only Thursday";
        schedule[5][0] = "Friday";
        schedule[5][1] = "go to the bar; drink some wine";
        schedule[6][0] = "Saturday";
        schedule[6][1] = "never-mind it's Saturday, continue watch Wednesday on Netflix; drink some wine; play Ragnarok: God of War on your PS5";
        Scanner in = new Scanner(System.in);
        do {
            System.out.println("Please, input the day of the week:");
            String userInput = in.nextLine();
            String checkedUserInput = checkUserInput(userInput, schedule);
            switch (checkedUserInput) {
                case "sunday": {
                    printChores(schedule[0][1]);
                    break;
                }
                case "monday": {
                    printChores(schedule[1][1]);
                    break;
                }
                case "tuesday": {
                    printChores(schedule[2][1]);
                    break;
                }
                case "thursday": {
                    printChores(schedule[4][1]);
                    break;
                }
                case "friday": {
                    printChores(schedule[5][1]);
                    break;
                }
                case "saturday": {
                    printChores(schedule[6][1]);
                    break;
                }
                case "exit": {
                    System.out.println("Have a nice and peaceful day!");
                    return;
                }
                case "reschedule sunday": {
                    rescheduleChores(schedule[0]);
                    break;
                }
                case "reschedule monday": {
                    rescheduleChores(schedule[1]);
                    break;
                }
                case "reschedule tuesday": {
                    rescheduleChores(schedule[2]);
                    break;
                }
                case "reschedule wednesday": {
                    rescheduleChores(schedule[3]);
                    break;
                }
                case "reschedule thursday": {
                    rescheduleChores(schedule[4]);
                    break;
                }
                case "reschedule friday": {
                    rescheduleChores(schedule[5]);
                    break;
                }
                case "reschedule saturday": {
                    rescheduleChores(schedule[6]);
                    break;
                }
                default: {
                    System.out.println("Sorry, I don't understand you, please try again");
                    break;
                }
            }
        } while (true);
    }

    public static String checkUserInput(String userInput, String[][] schedule) {
        userInput = userInput.trim().toLowerCase();
        if (userInput.equals("exit")) {
            return "exit";
        }
        for (String[] dayChores : schedule) {
            if (userInput.equalsIgnoreCase(dayChores[0].toLowerCase())) {
                return userInput;
            }
        }
        String[] arrayFromUserInput = userInput.split(" ");
        if (arrayFromUserInput[0].trim().equalsIgnoreCase("reschedule") ||
                arrayFromUserInput[0].trim().equalsIgnoreCase("change")) {
            for (String[] dayChores : schedule) {
                if (arrayFromUserInput[1].trim().equalsIgnoreCase(dayChores[0].toLowerCase())) {
                    return "reschedule ".concat(arrayFromUserInput[1].trim());
                }
            }
        }
        return "Sorry, I don't understand you, please try again";
    }

    public static void printChores(String chores) {
        String[] arrayOfChores = chores.split(";");
        for (String chore : arrayOfChores) {
            System.out.println("- " + chore.trim() + ";");
        }
    }

    public static void rescheduleChores(String[] day) {
        Scanner in = new Scanner(System.in);
        System.out.printf("Please, input new tasks for %s.\n", day[0]);
        String chores = in.nextLine();
        day[1] = chores;
        System.out.printf("New chores for %s are:\n", day[0]);
        printChores(chores);
    }
}