import happyFamily.*;
import happyFamily.humans.Human;
import happyFamily.humans.HumanCreator;
import happyFamily.humans.Man;
import happyFamily.humans.Women;
import happyFamily.pets.Fish;
import happyFamily.pets.Pet;
import happyFamily.pets.RoboCat;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Map<DayOfWeek, String> schedule = new HashMap<>();
        schedule.put(DayOfWeek.SUNDAY, "find a new hobby");
        schedule.put(DayOfWeek.MONDAY, "leave that hobby couse you need to earn money");
        schedule.put(DayOfWeek.THURSDAY, "sleep");
        schedule.put(DayOfWeek.WEDNESDAY, "earn more money");
        schedule.put(DayOfWeek.THURSDAY, "find a friend");
        schedule.put(DayOfWeek.FRIDAY, "go to the bar");
        schedule.put(DayOfWeek.SATURDAY, "relax");

        Pet robocat = new RoboCat("Robocat");
        System.out.println(robocat);

        Pet fish = new Fish("Fish", (byte) 2, 70, new HashSet<String>(Set.of("funny", "swim")));
        System.out.println(fish);
        fish.respond();

        Women mother = new Women("Mother", "Mother", 1990, 78);
        Man father = new Man("Father", "Father", 1980, 87);
        Family family = new Family<>(mother, father);
        Human kid = HumanCreator.bornChild(family);
        System.out.println(kid);

        family.addPet(fish);
        family.addPet(robocat);
        family.greetPet();

    }
}