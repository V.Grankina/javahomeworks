package happyFamily.pets;

import happyFamily.Species;

import java.util.Set;

public class Fish extends Pet {

    public Fish() {
        this.setSpecies(Species.FISH);
    }

    public Fish(String nickname) {
        super(nickname);
        this.setSpecies(Species.FISH);
    }

    public Fish(String nickname, byte age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        this.setSpecies(Species.FISH);
    }

    @Override
    public void respond() {
        System.out.println("Буль-буль");
    }
}
