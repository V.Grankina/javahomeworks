package happyFamily.humans;

import happyFamily.Family;

import java.util.*;

public interface HumanCreator {
    List<String> namesForGirls = new ArrayList<>(Arrays.asList("Maya", "Kate", "Samanta"));
    List<String> namesForBoys = new ArrayList<>(Arrays.asList("Ivan", "Piter", "Nikola"));

    public static Human bornChild(Family family) {
        Human mother = family.getMother();
        Human father = family.getFather();
        int childIq = (mother.getIq() + father.getIq()) / 2;
        Calendar calendar = new GregorianCalendar();
        int year = calendar.get(Calendar.YEAR);
        Random random = new Random();
        Human child;
        if (random.nextBoolean()){
            child = new Women(namesForGirls.get(random.nextInt(2)), father.getSurname(), year, mother, father);
        } else {
            child = new Man(namesForBoys.get(random.nextInt(2)), father.getSurname(), year, mother, father);
        }
        child.setIq(childIq);
        child.setFamily(family);
        return child;

    }
}
