package happyFamily.humans;

import happyFamily.DayOfWeek;
import happyFamily.Family;

import java.util.Map;
import java.util.Objects;

public class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Human mother;
    private Human father;
    private Map <DayOfWeek, String> schedule;
    Family family;

    {
        name = "Unknown";
        surname = "Unknown";

    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public Human() {
    }

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, Human mother, Human father) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.mother = mother;
        this.father = father;
    }

    public Human(String name, String surname, int year, int iq) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
    }

    public Human(String name, String surname, int year, int iq, Human mother, Human father, Map <DayOfWeek, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.mother = mother;
        this.father = father;
        this.schedule = schedule;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(short year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Map <DayOfWeek, String> getSchedule() {
        return schedule;
    }

    public void setSchedule(Map <DayOfWeek, String> schedule) {
        this.schedule = schedule;
    }

    public String getFullName() {
        return String.format("%s %s", this.getName(), this.getSurname());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return getYear() == human.getYear() && getName().equals(human.getName()) && getSurname().equals(human.getSurname()) && Objects.equals(getMother(), human.getMother()) && Objects.equals(getFather(), human.getFather());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getSurname(), getYear(), getMother(), getFather());
    }

    @Override
    public String toString() {
        return String.format("Human {name = '%s', surname = '%s', year = %d, iq = %d, mother = %s, father = %s}\n",
                this.getName(),
                this.getSurname(),
                this.getYear(),
                this.getIq(),
                this.getMother() == null ? "null" : this.getMother().getFullName(),
                this.getFather() == null ? "null" : this.getFather().getFullName());
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println(this.toString());
        super.finalize();
    }
}
