import happyFamily.Family;
import happyFamily.Human;
import happyFamily.Pet;

public class Main {
    public static void main(String[] args) {
        String [] habits = {"Eat", "Sleep"};
        byte age = 2;
        Pet pet = new Pet();
        Pet pet1 = new  Pet("dog", "Spike");
        Pet pet3 = new  Pet("cat", "Trixie", age, 55, habits);

        Human mother = new Human();
        Human father = new Human();
        Family family = new Family(mother, father);
        family.setPet(pet);
        System.out.println(family.toString());

        Human mother1 = new Human("Olya", "Petrik", 1992);
        Human father1 = new Human("Jame", "Mask", 1987, father, mother);
        Human kid1 = new Human("Kid1", "Kid1", 2022);
        Human kid2 = new Human("Kid2", "Kid2", 2021);
        Human kid3 = new Human("Kid3", "Kid3", 2022);
        Human kid4 = new Human("Kid4", "Kid4", 2021);
        Family family1 = new Family(mother1, father1);
        family1.addChild(kid1);
        family1.addChild(kid2);
        family1.addChild(kid3);
        family1.addChild(kid4);
        family1.setPet(pet3);
        System.out.println(family1.toString());
        family1.describePet();
        family1.greetPet();
        family1.deleteChild(kid3);
        System.out.println(family1.toString());
        family1.feedPet(true);
        family1.feedPet(true);
        family1.feedPet(false);

    }
}