package structure;

import happyFamily.Family;
import happyFamily.humans.Human;
import happyFamily.humans.HumanCreator;
import happyFamily.humans.Man;
import happyFamily.humans.Women;
import happyFamily.pets.Pet;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

public class FamilyService {
    private FamilyDao familyDao;

    public FamilyService(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public List<Family> getAllFamilies (){
        return familyDao.getAllFamilies();
    }
    public void displayAllFamilies(){
        Stream.of(familyDao.getAllFamilies()).forEach(System.out::println);
    }
    public List<Family> getFamiliesBiggerThan (int familyMembers){
        List<Family> families = familyDao.getAllFamilies();
        return families.stream().filter(family -> family.countFamily() > familyMembers).toList();
    }
    public List<Family> getFamiliesLessThan (int familyMembers){
        List<Family> families = familyDao.getAllFamilies();
        return families.stream().filter(family -> family.countFamily() < familyMembers).toList();

    }
    public int countFamiliesWithMemberNumber (int familyMembers){
        List<Family> families = familyDao.getAllFamilies();
        return (int) families.stream().filter(family -> family.countFamily() == familyMembers).count();
    }
    public void createNewFamily (Women Parent1, Man Parent2){
        Family family = new Family(Parent1, Parent2);
        familyDao.saveFamily(family);
    }
    public boolean deleteFamilyByIndex(int index){
        return familyDao.deleteFamily(index);
    }
    public Family bornChild (Family family){
        if (familyDao.checkIfContainsFamily(family)){
            HumanCreator.bornChild(family);
        }
        return family;
    }
    public void adoptChild(Family family, Human kid){
        if (familyDao.checkIfContainsFamily(family)){
            family.addChild(kid);
        }
    }
    public void deleteAllChildrenOlderThen( int age){
        List<Family> families = familyDao.getAllFamilies();
        families.forEach(family -> family.deleteAllChildrenOlderThen(age));
    }
    public int count (){
        return familyDao.getAllFamilies().size();
    }
    public  Family getFamilyById (int id){
        return familyDao.getFamilyByIndex(id);
    }
    public Set <Pet> getPets (int index){
        Family family = familyDao.getFamilyByIndex(index);
        return family.getPets();
    }

    public void addPet (int index, Pet pet){
        Family family = familyDao.getFamilyByIndex(index);
        family.addPet(pet);
    }
}
