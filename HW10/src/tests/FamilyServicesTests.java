package tests;
import happyFamily.Family;
import happyFamily.humans.Human;
import happyFamily.humans.Man;
import happyFamily.humans.Women;
import happyFamily.pets.RoboCat;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import structure.CollectionFamilyDao;
import structure.FamilyDao;
import structure.FamilyService;

import java.util.ArrayList;
import java.util.List;

public class FamilyServicesTests {
    private FamilyService module;
    private Women mother = new Women("Mother", "Mother", "01/01/1990");
    private Man father = new Man("Father", "Father", "01/01/1980");
    private Human kid1 = new Human("kid1", "kid1", "01/01/2000");
    private Human kid2 = new Human("kid2", "kid2", "01/01/2002");
    private Human kid3 = new Human("kid3", "kid3", "01/01/2005");
    private List<Human> children = new ArrayList<>(List.of(kid1, kid2, kid3));
    private Family family1 = new Family(mother,father, children);
    private Family family2 = new Family(mother,father);
    private List<Family> families = new ArrayList<>(List.of(family1,family2));
    private CollectionFamilyDao familyDao = new CollectionFamilyDao(families);

    @BeforeEach
    public void setUp() {module = new FamilyService(familyDao);};

    @Test
    void testGetAllFamilies(){
        List<Family> families = module.getAllFamilies();
        Assertions.assertEquals(2, families.size());
    }

    @Test
    void testGetFamiliesLessThan() {
        List<Family> familiesLessThan = module.getFamiliesLessThan(3);
        Assertions.assertEquals(1, familiesLessThan.size());
    }

    @Test
    void testCountFamiliesWithMemberNumber() {
        int familiesWithMemberNumber = module.countFamiliesWithMemberNumber(2);
        Assertions.assertEquals(1, familiesWithMemberNumber);
    }

    @Test
    void testCreateNewFamily() {
        Women parent1 = new Women();
        Man parent2 = new Man();
        module.createNewFamily(parent1, parent2);
        Assertions.assertEquals(module.getFamilyById(2).getMother(), parent1);
        Assertions.assertEquals(module.getFamilyById(2).getFather(), parent2);
    }

    @Test
    void testDeleteFamilyByIndex() {
        boolean result = module.deleteFamilyByIndex(3);
        Assertions.assertFalse(result);
        boolean result1 = module.deleteFamilyByIndex(1);
        Assertions.assertTrue(result1);;
        Assertions.assertEquals(1, module.count());
    }

    @Test
    void testBurnChild() {
        module.bornChild(family2);
        Assertions.assertEquals(1, family2.getChildren().size());
    }

    @Test
    void testDeleteAllChildrenOlderThen() {
        module.deleteAllChildrenOlderThen(18);
        System.out.println(family1.getChildren());
        Assertions.assertEquals(2, family1.getChildren().size());
    }

    @Test
    void testCount() {
        Assertions.assertEquals(families.size(), module.count());
    }

    @Test
    void testAddPet() {
        module.addPet(0, new RoboCat());
        Assertions.assertEquals(1, module.getPets(0).size());
    }
}
