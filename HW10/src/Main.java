import happyFamily.*;
import happyFamily.DayOfWeek;
import happyFamily.humans.Human;
import happyFamily.humans.HumanCreator;
import happyFamily.humans.Man;
import happyFamily.humans.Women;
import happyFamily.pets.Fish;
import happyFamily.pets.Pet;
import happyFamily.pets.RoboCat;
import structure.CollectionFamilyDao;
import structure.FamilyController;
import structure.FamilyService;

import java.sql.SQLOutput;
import java.text.SimpleDateFormat;
import java.time.*;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        Map<DayOfWeek, String> schedule = new HashMap<>();
        schedule.put(DayOfWeek.SUNDAY, "find a new hobby");
        schedule.put(DayOfWeek.MONDAY, "leave that hobby couse you need to earn money");
        schedule.put(DayOfWeek.THURSDAY, "sleep");
        schedule.put(DayOfWeek.WEDNESDAY, "earn more money");
        schedule.put(DayOfWeek.THURSDAY, "find a friend");
        schedule.put(DayOfWeek.FRIDAY, "go to the bar");
        schedule.put(DayOfWeek.SATURDAY, "relax");
        Pet robocat = new RoboCat("Robocat");
        Pet fish = new Fish("Fish", (byte) 2, 70, new HashSet<String>(Set.of("funny", "swim")));

        Women mother = new Women("Mother", "Mother", "01/01/1990", 78);
        Man father = new Man("Father", "Father", "01/01/1990", 87);

        Human kid1 = new Human("kid1", "kid1", "01/03/2000");
        Human kid2 = new Human("kid2", "kid2", "01/09/2003");
        Human kid3 = new Human("kid3", "kid3", "25/01/2005");
        Family family1 = new Family(mother, father);
        family1.addPet(fish);
        family1.addPet(robocat);
        family1.addChild(kid2);
        family1.addChild(kid1);
        family1.addChild(kid3);

        Women parent1 = new Women();
        Man parent2 = new Man();
        Family family2 = new Family(parent1, parent2);

        List<Family> families = new ArrayList<>(List.of(family1, family2));
        CollectionFamilyDao familyDao = new CollectionFamilyDao(families);
        FamilyService familyService = new FamilyService(familyDao);
        FamilyController familyController = new FamilyController(familyService);

        familyController.displayAllFamilies();
        System.out.println("Families bigger than 2");
        System.out.println(familyController.getFamiliesBiggerThan(2));
        System.out.println("Families less than 3");
        System.out.println(familyController.getFamiliesLessThan(3));
        System.out.println("Number of Families with 2 members");
        System.out.println(familyController.countFamiliesWithMemberNumber(2));
        // Create a new family and add it to the Db
        familyController.createNewFamily(new Women(), new Man());
        // Find this new family by id
        Family family3 = familyController.getFamilyById(2);
        // Burn a child!
        familyController.bornChild(family3);
        // Adopt a child
        familyController.adoptChild(family3, new Human());
        System.out.println("New family:");
        System.out.println(family3);
        // delete new family
        familyController.deleteFamilyByIndex(2);
        System.out.println(familyController.getAllFamilies());
        // delete all children older then 18
        familyController.deleteAllChildrenOlderThen(18);
        // add a pet to 2-nd family
        familyController.addPet(1, robocat);
        familyController.displayAllFamilies();

    }
}