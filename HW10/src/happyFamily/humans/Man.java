package happyFamily.humans;

import happyFamily.DayOfWeek;

import java.util.Map;

final public class Man extends Human{
    public Man() {
    }

    public Man(String name, String surname, String birthDate) {
        super(name, surname, birthDate);
    }

    public Man(String name, String surname, String birthDate, Human mother, Human father) {
        super(name, surname, birthDate, mother, father);
    }

    public Man(String name, String surname, long birthDate, Human mother, Human father) {
        super(name, surname, birthDate, mother, father);
    }

    public Man(String name, String surname, String birthdate, int iq) {
        super(name, surname, birthdate, iq);
    }

    public Man(String name, String surname, String birthdate, int iq, Human mother, Human father, Map<DayOfWeek, String> schedule) {
        super(name, surname, birthdate, iq, mother, father, schedule);
    }

    public void DriveTheCar(){
        System.out.println("Би бип");
    }
}
