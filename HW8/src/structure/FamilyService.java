package structure;

import happyFamily.Family;
import happyFamily.humans.Human;
import happyFamily.humans.HumanCreator;
import happyFamily.humans.Man;
import happyFamily.humans.Women;
import happyFamily.pets.Pet;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class FamilyService {
    private FamilyDao familyDao;

    public FamilyService(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public List<Family> getAllFamilies (){
        return familyDao.getAllFamilies();
    }
    public void displayAllFamilies(){
        System.out.println(familyDao.getAllFamilies());
    }
    public List<Family> getFamiliesBiggerThan (int familyMembers){
        List<Family> familiesBiggerThan = new ArrayList<>();
        List<Family> families = familyDao.getAllFamilies();
        families.forEach(family -> {
            if (family.countFamily() > familyMembers){
                familiesBiggerThan.add(family);
            }
        });
        return familiesBiggerThan;
    }
    public List<Family> getFamiliesLessThan (int familyMembers){
        List<Family> familiesLessThan = new ArrayList<>();
        List<Family> families = familyDao.getAllFamilies();
        families.forEach(family -> {
            if (family.countFamily() < familyMembers){
                familiesLessThan.add(family);
            }
        });
        return familiesLessThan;
    }
    public int countFamiliesWithMemberNumber (int familyMembers){
        Integer familiesWithMemberNumber = 0;
        List<Family> families = familyDao.getAllFamilies();
        for (Family family : families) {
            if (family.countFamily() == familyMembers) {
                familiesWithMemberNumber++;
            }
        }
        return familiesWithMemberNumber;
    }
    public void createNewFamily (Women Parent1, Man Parent2){
        Family family = new Family(Parent1, Parent2);
        familyDao.saveFamily(family);
    }
    public boolean deleteFamilyByIndex(int index){
        return familyDao.deleteFamily(index);
    }
    public Family bornChild (Family family){
        if (familyDao.checkIfContainsFamily(family)){
            HumanCreator.bornChild(family);
        }
        return family;
    }
    public void adoptChild(Family family, Human kid){
        if (familyDao.checkIfContainsFamily(family)){
            family.addChild(kid);
        }
    }
    public void deleteAllChildrenOlderThen( int age){
        List<Family> families = familyDao.getAllFamilies();
        for (Family family : families) {
            family.deleteAllChildrenOlderThen(age);
        }
    }
    public int count (){
        return familyDao.getAllFamilies().size();
    }
    public  Family getFamilyById (int id){
        return familyDao.getFamilyByIndex(id);
    }
    public Set <Pet> getPets (int index){
        Family family = familyDao.getFamilyByIndex(index);
        return family.getPets();
    }

    public void addPet (int index, Pet pet){
        Family family = familyDao.getFamilyByIndex(index);
        family.addPet(pet);
    }
}
