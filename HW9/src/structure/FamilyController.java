package structure;

import happyFamily.Family;
import happyFamily.humans.Human;
import happyFamily.humans.Man;
import happyFamily.humans.Women;
import happyFamily.pets.Pet;

import java.util.List;
import java.util.Set;

public class FamilyController {
    private FamilyService familyService;

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;
    }

    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    public void displayAllFamilies() {
        familyService.displayAllFamilies();
    }

    public List<Family> getFamiliesBiggerThan(int familyMembers) {
        return familyService.getFamiliesBiggerThan(familyMembers);
    }

    public List<Family> getFamiliesLessThan(int familyMembers) {
        return familyService.getFamiliesLessThan(familyMembers);
    }

    public int countFamiliesWithMemberNumber(int familyMembers) {
        return familyService.countFamiliesWithMemberNumber(familyMembers);
    }

    public void createNewFamily(Women Parent1, Man Parent2) {
        familyService.createNewFamily(Parent1, Parent2);
    }

    public boolean deleteFamilyByIndex(int index) {
        return familyService.deleteFamilyByIndex(index);
    }

    public Family bornChild(Family family) {
        return familyService.bornChild(family);
    }

    public void adoptChild(Family family, Human kid) {
        familyService.adoptChild(family, kid);
    }

    public void deleteAllChildrenOlderThen(int age) {
        familyService.deleteAllChildrenOlderThen(age);
    }

    public int count() {
        return familyService.count();
    }

    public Family getFamilyById(int id) {
        return familyService.getFamilyById(id);
    }

    public Set<Pet> getPets(int index) {
        return familyService.getPets(index);
    }

    public void addPet(int index, Pet pet) {
        familyService.addPet(index, pet);
    }
}