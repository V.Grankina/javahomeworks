package happyFamily;

public enum Species {
    CAT (false, 4, true),
    DOG (false, 4, true),
    PARROT (true, 2, false),
    SNAKE(false, 0, false),
    FISH(false,0,false),
    ROBOCAT(true,4,false),
    UNKNOWN(false, 0, false);

    boolean canFly;
    int numberOfLegs;
    boolean hasFur;

    Species(boolean canFly, int numberOfLegs, boolean hasFur) {
        this.canFly = canFly;
        this.numberOfLegs = numberOfLegs;
        this.hasFur = hasFur;
    }

    @Override
    public String toString() {
        return super.toString() + "{" +
                "canFly=" + canFly +
                ", numberOfLegs=" + numberOfLegs +
                ", hasFur=" + hasFur +
                "} ";
    }
}
