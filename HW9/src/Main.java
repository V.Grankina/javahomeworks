import happyFamily.*;
import happyFamily.DayOfWeek;
import happyFamily.humans.Human;
import happyFamily.humans.HumanCreator;
import happyFamily.humans.Man;
import happyFamily.humans.Women;
import happyFamily.pets.Fish;
import happyFamily.pets.Pet;
import happyFamily.pets.RoboCat;
import structure.CollectionFamilyDao;
import structure.FamilyController;
import structure.FamilyService;

import java.sql.SQLOutput;
import java.text.SimpleDateFormat;
import java.time.*;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        Map<DayOfWeek, String> schedule = new HashMap<>();
        schedule.put(DayOfWeek.SUNDAY, "find a new hobby");
        schedule.put(DayOfWeek.MONDAY, "leave that hobby couse you need to earn money");
        schedule.put(DayOfWeek.THURSDAY, "sleep");
        schedule.put(DayOfWeek.WEDNESDAY, "earn more money");
        schedule.put(DayOfWeek.THURSDAY, "find a friend");
        schedule.put(DayOfWeek.FRIDAY, "go to the bar");
        schedule.put(DayOfWeek.SATURDAY, "relax");

        Human human = new Human("Vik", "Grnk", "19/03/1992", 98);
        System.out.println(human.getBirthDate());
        System.out.println(human.describeAge());
        System.out.println(human);

    }
}