package tests;

import happyFamily.Family;
import happyFamily.humans.Human;
import happyFamily.humans.HumanCreator;
import happyFamily.humans.Man;
import happyFamily.humans.Women;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

public class HumanCreatorTests {

    private Family module;
    private Women mother = new Women("Mother", "Mother", 1990, 100);
    private Man father = new Man("Father", "Father", 1980, 50);

    @BeforeEach
    public void Setup(){
        module = new Family(mother, father);
    }
    @Test
    public void bornChildTest(){
        Human child = HumanCreator.bornChild(module);
        Assertions.assertEquals(child.getIq(), 75);
    }
}
