package structure;
import happyFamily.Family;
import java.util.List;

public interface FamilyDao {
     List<Family> getAllFamilies();
     Family getFamilyByIndex(int index);
     boolean checkIfContainsFamily(Family family);
     boolean deleteFamily(int index);
     boolean deleteFamily(Family family);
     void saveFamily(Family family);
     void updateAllFamilies(List<Family> families);
}
