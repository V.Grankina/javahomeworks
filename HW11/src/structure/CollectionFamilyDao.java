package structure;

import happyFamily.Family;

import java.util.List;

public class CollectionFamilyDao implements FamilyDao{

    private List<Family> FamilyList;

    public CollectionFamilyDao(List<Family> familyList) {
        FamilyList = familyList;
    }

    @Override
    public List<Family> getAllFamilies() {
        return FamilyList;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        return FamilyList.get(index);
    }

    @Override
    public boolean deleteFamily(int index) {
        if (index < 0 || index >= FamilyList.size()){
            return false;
        }
        FamilyList.remove(index);
        return true;
    }

    @Override
    public boolean deleteFamily(Family family) {
        return FamilyList.remove(family);
    }

    @Override
    public void saveFamily(Family family) {
        FamilyList.add(family);
    }


    @Override
    public boolean checkIfContainsFamily(Family family) {
        return FamilyList.contains(family);
    }

    @Override
    public void updateAllFamilies(List<Family> families) {
        this.FamilyList = families;
    }
}
