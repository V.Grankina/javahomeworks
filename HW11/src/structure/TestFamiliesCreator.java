package structure;

import happyFamily.Family;
import happyFamily.humans.Man;
import happyFamily.humans.Women;
import happyFamily.pets.Dog;
import happyFamily.pets.DomesticCat;

import java.util.ArrayList;
import java.util.List;

public class TestFamiliesCreator {
    public  static List<Family> create () {
        List <Family> families = new ArrayList<>();
        //Family1
        Women mother1 = new Women("Olga", "Saenko","24/10/1972", 89);
        Man father1 = new Man("Ruslan", "Saienko", "18/08/1973", 98);
        Women girl1 = new Women("Sofia", "Saienko", "10/08/2010", 56);
        Dog dog = new Dog("Trixiy", 3, 67);
        Family family1 = new Family(mother1,father1);
        family1.addChild(girl1);
        family1.addPet(dog);
        families.add(family1);
        //Family2
        Women mother2 = new Women("Katya", "Gricay","22/02/1987", 67);
        Man father2 = new Man("Oleg", "Gricay", "13/02/1976", 78);
        Women girl2 = new Women("Pita", "Gricay", "12/01/2022", 65);
        DomesticCat cat = new DomesticCat("Paolo", 3, 67);
        Family family2 = new Family(mother2,father2);
        family2.addChild(girl2);
        family2.addPet(cat);
        families.add(family2);
        //Family3
        Women mother3 = new Women("Kika", "Polod","22/02/2000", 76);
        Man father3 = new Man("Koko", "Polod", "12/02/1999", 78);
        Family family3 = new Family(mother3,father3);
        families.add(family3);
        //Family4
        Women mother4 = new Women("Maria", "Molodec","22/02/1987", 45);
        Man father4 = new Man("Glib", "Molodec", "13/02/1976", 44);
        Women girl4 = new Women("Liza", "Molodec", "12/03/2012", 56);
        Man boy4 = new Man("Luk", "Molodec", "14/05/2015", 76);
        Man boy41 = new Man("Borya", "Molodec", "14/08/2020", 56);
        Family family4 = new Family(mother4,father4);
        family2.addChild(girl4);
        family2.addChild(boy41);
        family2.addChild(boy4);
        families.add(family4);
        return families;
    }
}
