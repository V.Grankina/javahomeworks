package tests;

import happyFamily.Family;
import happyFamily.humans.Human;
import happyFamily.humans.Man;
import happyFamily.humans.Women;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class FamilyTests {
    private Family module;
    private Women mother = new Women("Mother", "Mother", "01/01/1990");
    private Man father = new Man("Father", "Father", "01/01/1980");
    private Human kid1 = new Human("kid1", "kid1", "01/01/2000");
    private Human kid2 = new Human("kid2", "kid2", "01/01/2002");
    private Human kid3 = new Human("kid3", "kid3", "01/01/2005");

    private List<Human> children = new ArrayList<>(List.of(kid1, kid2, kid3));

    @BeforeEach
    public void setUp(){
        module = new Family(mother, father);
    }

    @Test
    public void testDeleteChild(){
        module.setChildren(this.children);
        module.deleteChild(kid1);
        int actual = module.getChildren().size();
        int expected = 2;
        Assertions.assertEquals(actual, expected);
    }

    @Test
    public void testDontDeleteChild(){
        module.setChildren(this.children);
        module.deleteChild(new Human());
        int actual = module.getChildren().size();
        int expected = 3;
        Assertions.assertEquals(actual, expected);
    }

    @Test
    public void testAddChild(){
        module.setChildren(this.children);
        module.addChild(new Human());
        int actual = module.getChildren().size();
        int expected = 4;
        Assertions.assertEquals(actual, expected);
    }
    @Test
    public void testCountFamily(){
        module.setChildren(this.children);
        int actual = module.countFamily();;
        int expected = 5;
        Assertions.assertEquals(actual, expected);
    }

}
