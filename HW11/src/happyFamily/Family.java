package happyFamily;

import happyFamily.humans.Human;
import happyFamily.humans.Man;
import happyFamily.humans.Women;
import happyFamily.pets.Pet;

import java.util.*;

public class Family {
    private final Women mother;
    private final Man father;
    private List<Human> children;

    private Set<Pet> pets;

    public Women getMother() {
        return mother;
    }

    public Man getFather() {
        return father;
    }

    public List<Human> getChildren() {
        return children;
    }

    public void setChildren(List<Human> children) {
        this.children = children;
    }

    public Set<Pet> getPets() {
        return this.pets;
    }

    public void setPets(Pet pet) {
        this.pets.add(pet);
    }

    public Family(Women mother, Man father) {
        this.mother = mother;
        this.father = father;
        this.mother.setFamily(this);
        this.father.setFamily(this);
        pets = new HashSet<>();
        children = new ArrayList<>();
    }

    public Family(Women mother, Man father, List<Human> children) {
        this.mother = mother;
        this.father = father;
        this.children = children;
        this.mother.setFamily(this);
        this.father.setFamily(this);
        pets = new HashSet<>();
    }

    public void prettyFormat() {
        String childrenString = "Children:";
        for(Human child: this.children){
            if (child instanceof Women) {
                childrenString =  childrenString + "\n    girl: ";
            } else {
                childrenString = childrenString + "\n    boy: ";
            }
            childrenString = childrenString + child.prettyFormat();
        }
        String pets = "Pets:\n     ";
        for(Pet pet:this.pets){
            pets = pets + pet.prettyFormat() + " ";
        }

        String pretty = "Family:\nFather: " + this.father.prettyFormat() +
                "\nMather: " + this.mother.prettyFormat() + "\n" + childrenString + "\n" + pets;
        System.out.println(pretty);
    }

    @Override
    public String toString() {
        return String.format("Father: %sMother: %sChildren: %s\nPet: %s",
                this.father.toString(),
                this.mother.toString(),
                this.children == null ? "null" : children,
                this.getPets() == null ? "null" : this.getPets().toString());
    }

    public void addPet(Pet pet) {
        this.pets.add(pet);
    }

    public void greetPet() {
        for (Pet pet : this.pets) {
            System.out.printf("Привет, %s\n", pet.getNickname());
        }
    }

    public void describePet() {
        for (Pet pet : this.pets) {
            int trickLevel = pet.getTrickLevel();
            String trickLevelString = trickLevel > 50 ? "очень хитрый" : "почти не хитрый";
            System.out.printf("У нас есть %s, ему %d лет, он %s\n", pet.getSpecies(), pet.getAge(), trickLevelString);
        }

    }

    public void addChild(Human child) {
        children.add(child);
        child.setFamily(this);
        child.setFather(this.father);
        child.setMother(this.mother);
    }

    public boolean deleteChild(Human child) {
        if (this.children == null) {
            return false;
        }
        return children.remove(child);
    }

    public boolean feedPet(boolean timeToFeed) {
        if (!timeToFeed) {
            System.out.printf("Думаю, животины не голодны.\n");
            return false;
        }
        for (Pet pet : this.pets) {
            Random random = new Random();
            int randomInt = random.nextInt(100);
            if (pet.getTrickLevel() < randomInt) {
                System.out.printf("Думаю, %s не голоден.\n", pet.getNickname());
                return false;
            } else {
                System.out.printf("Хм... покормлю ка я %s\n", pet.getNickname());
                return true;
            }
        }
        return false;
    }

    public int countFamily() {
        return 2 + (this.children.size());
    }

    public void deleteAllChildrenOlderThen(int age) {
        this.children =  this.children.stream().filter(child -> (System.currentTimeMillis() - child.getBirthDate()) < (long) age * 31557600000L).toList();
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println(this.toString());
        super.finalize();
    }

}
