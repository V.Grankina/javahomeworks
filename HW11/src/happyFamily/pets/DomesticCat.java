package happyFamily.pets;

import happyFamily.Species;

import java.util.Set;

public class DomesticCat extends Pet implements Fouling{

    public DomesticCat(String nickname, int age, int trickLevel) {
        super(nickname, age, trickLevel);
        this.setSpecies(Species.CAT);
    }

    public DomesticCat(String nickname) {
        super(nickname);
        this.setSpecies(Species.CAT);
    }

    public DomesticCat(String nickname, byte age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        this.setSpecies(Species.CAT);
    }

    @Override
    public void foul() {
        System.out.println("Я кот это не мои проблемы. Кто кота чесал, тому и убираться");
    }

    @Override
    public void respond() {
        System.out.println("Мяу мяу");
    }
}
