package happyFamily.pets;

import happyFamily.Species;

import java.util.Set;

abstract public class Pet {
    private Species species;
    private String nickname;
    private byte age;
    private int trickLevel;
    private Set <String> habits;
    {
//        species = Species.UNKNOWN;
        nickname = "Unknown";
    }
    public Pet() {
    }

    public Pet (String nickname) {
        this.nickname = nickname;
    }

    public Pet(String nickname, int age, int trickLevel) {
        this.nickname = nickname;
        this.age = (byte) age;
        this.trickLevel = trickLevel;
    }

    public Pet(String nickname, byte age, int trickLevel, Set <String> habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public void eat() {
        System.out.println("Я кушаю!");
    }

    abstract public void respond();

//    public void foul() {
//        System.out.println("Нужно хорошо замести следы...");
//    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(byte age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public Set <String>  getHabits() {
        return habits;
    }

    public void setHabits(Set <String> habits) {
        this.habits = habits;
    }

    public String prettyFormat(){
        return "{" +
                "species='" + this.species.name() + '\'' +
                ", nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                "}";
    }

    @Override
    public String toString() {
        return "Pet{" +
                "species='" + species.toString() + '\'' +
                ", nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + habits +
                "}";
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println(this.toString());
        super.finalize();
    }
}
