package happyFamily.pets;

import happyFamily.Species;

import java.util.Set;

public class Dog extends Pet implements Fouling{

    public Dog(String nickname, int age, int trickLevel) {
        super(nickname, age, trickLevel);
        this.setSpecies(Species.DOG);
    }

    public Dog(String nickname) {
        super(nickname);
        this.setSpecies(Species.DOG);
    }

    public Dog(String nickname, byte age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        this.setSpecies(Species.DOG);
    }

    @Override
    public void foul() {
        System.out.println("Это кот сделал");
    }

    @Override
    public void respond() {
        System.out.println("Гав гав гав");
    }
}
