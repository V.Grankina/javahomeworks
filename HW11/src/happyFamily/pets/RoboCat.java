package happyFamily.pets;

import happyFamily.Species;

import java.util.Set;

public class RoboCat extends Pet{

    public RoboCat() {
        this.setSpecies(Species.ROBOCAT);
    }

    public RoboCat(String nickname) {
        super(nickname);
        this.setSpecies(Species.ROBOCAT);
    }

    public RoboCat(String nickname, int age, int trickLevel) {
        super(nickname, age, trickLevel);
        this.setSpecies(Species.ROBOCAT);
    }

    public RoboCat(String nickname, byte age, int trickLevel, Set <String> habits) {
        super(nickname, age, trickLevel, habits);
        this.setSpecies(Species.ROBOCAT);
    }

    @Override
    public void respond() {
        System.out.println("0101100101");
    }
}
