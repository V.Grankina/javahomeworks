package happyFamily.humans;

import happyFamily.DayOfWeek;
import happyFamily.Family;

import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class Human {
    private String name;
    private String surname;

    private long birthDate;
    private int iq;
    private Human mother;
    private Human father;
    private Map <DayOfWeek, String> schedule;
    Family family;

    {
        name = "Unknown";
        surname = "Unknown";

    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public Human() {
    }

    public Human(String name, String surname, long birthDate, Human mother, Human father) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.mother = mother;
        this.father = father;
    }

    public Human(String name, String surname, String birthDate) {
        this.name = name;
        this.surname = surname;
        this.setBirthDate(birthDate);
    }

    public Human(String name, String surname, String birthDate, Human mother, Human father) {
        this.name = name;
        this.surname = surname;
        this.setBirthDate(birthDate);
        this.mother = mother;
        this.father = father;
    }


    public Human(String name, String surname, String birthdate, int iq) {
        this.name = name;
        this.surname = surname;
        this.setBirthDate(birthdate);
        this.iq = iq;
    }

    public Human(String name, String surname, String birthdate, int iq, Human mother, Human father, Map <DayOfWeek, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.setBirthDate(birthDate);
        this.iq = iq;
        this.mother = mother;
        this.father = father;
        this.schedule = schedule;
    }

    public long getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(long birthDate) {
        this.birthDate = birthDate;
    }
    public void setBirthDate(String birthDate) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");
        LocalDate localDate = LocalDate.parse(birthDate,formatter);
        Instant instant = localDate.atStartOfDay(ZoneId.systemDefault()).toInstant();
        this.birthDate = instant.toEpochMilli();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        LocalDateTime date = LocalDateTime.ofInstant(Instant.ofEpochMilli(this.birthDate), ZoneId.systemDefault());
        return date.getYear();
    }


    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Map <DayOfWeek, String> getSchedule() {
        return schedule;
    }

    public void setSchedule(Map <DayOfWeek, String> schedule) {
        this.schedule = schedule;
    }

    public String getFullName() {
        return String.format("%s %s", this.getName(), this.getSurname());
    }

    public String describeAge(){
        LocalDateTime date = LocalDateTime.ofInstant(Instant.ofEpochMilli(this.birthDate), ZoneId.systemDefault());
        return date.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
    }

    public String prettyFormat(){
        return this.name + " " + this.surname + ", date of birth:" + this.describeAge() + ", IQ: " + this.iq;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return getYear() == human.getYear() && getName().equals(human.getName()) && getSurname().equals(human.getSurname()) && Objects.equals(getMother(), human.getMother()) && Objects.equals(getFather(), human.getFather());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getSurname(), getYear(), getMother(), getFather());
    }

    @Override
    public String toString() {
        return String.format("Human {name = '%s', surname = '%s', birthDate = %s, iq = %d, mother = %s, father = %s}\n",
                this.getName(),
                this.getSurname(),
                this.describeAge(),
                this.getIq(),
                this.getMother() == null ? "null" : this.getMother().getFullName(),
                this.getFather() == null ? "null" : this.getFather().getFullName());
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println(this.toString());
        super.finalize();
    }
}
