import happyFamily.*;
import structure.CollectionFamilyDao;
import structure.FamilyController;
import structure.FamilyService;

import java.util.*;

public class Main {
    public static void main(String[] args) {

        List<Family> families = new ArrayList<>();
        CollectionFamilyDao familyDao = new CollectionFamilyDao(families);
        FamilyService familyService = new FamilyService(familyDao);
        FamilyController familyController = new FamilyController(familyService);

        familyController.start();
    }
}