package exceptions;

public class ToMuchKidsException extends RuntimeException{

    public ToMuchKidsException() {
    }

    public ToMuchKidsException(String message) {
        super(message);
    }
}
